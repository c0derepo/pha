/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
/**
 *
 * Token.java
 *
 * @author c0der 4c.app.testing@gmail.com
 *
 * 15 Aug 2018
 *
 */
enum Token {

    EMPTY (Color.WHITE), BLUE (Color.BLUE), RED(Color.RED);

    private Color color;

    Token(Color color) {

        this.color = color;
    }

	public Color getColor() {

		return color;
	}

}