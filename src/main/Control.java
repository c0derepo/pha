/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.SwingUtilities;

/**
 *
 * Control.java
 *
 * @author c0der 4c.app.testing@gmail.com
 *
 * 15 Aug 2018
 *
 */
public class Control {

	private Model model;
    private View view;
    private Token lastToken;

    public Control(Model model, View view) {

    	this.model = model;
        this.view = view;
        lastToken = Token.BLUE;

        view.createGrid(model.getRows());

        view.addPropertyChangeListener(new ViewListener());
        model.addPropertyChangeListener(model.getToken(), new ModelListener());
        view.start();
    }

    //a listener added to view panel to listen to property  changes events
    //fired by the mouse listener of each cell
    private class ViewListener implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(View.CELL_SELECTION)) {
                int row = view.getSelectedRow();
                int col = view.getSelectedCol();

                Token token = model.getToken(row, col);
                if (token == Token.EMPTY) {

                	lastToken = (lastToken == Token.BLUE) ?
                			Token.RED : Token.BLUE;
                    token = lastToken;
                }
                model.setToken(token, row, col);
            }
        }
    }

    //listener added to model to listen to token changes. used to updated view
    //when token changes
    private class ModelListener implements PropertyChangeListener {

    	@Override
        public void propertyChange(PropertyChangeEvent evt) {

        	IndexedPropertyChangeEvent iEvt = (IndexedPropertyChangeEvent)evt;
            int index = iEvt.getIndex();
            int row = index / model.getRows();
            int col = index % model.getCols();
            Token token = (Token) evt.getNewValue();

            SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

		            view.setCell(token, row, col);
		            List<Cell> path = model.getPath();
		            //ignore path if null, empty or encloses no cell
		            if((path == null) || path.isEmpty()
		            					|| model.getContainedWithin().isEmpty()) {
						return;
					}
		            view.addPath(path);
		            view.refresh();
				}
			});
        }
    }
}