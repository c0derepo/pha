/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import java.util.Arrays;

/**
 *
 * Cell.java
 *
 * @author c0der 4c.app.testing@gmail.com
 *
 * 15 Aug 2018
 *
 */
public class Cell {

	private int row, col;
	private Token token;

	Cell(int row, int col) {

		this(row, col, Token.EMPTY);
	}

	Cell(int row, int col, Token token) {

		this.row = Math.abs(row); //to allow only positive addresses
		this.col = Math.abs(col);
		this.token = (token == null) ? Token.EMPTY : token;
	}

	int getRow() {
		return row;
	}

	int getCol() {
		return col;
	}

	Token getToken() {
		return token;
	}

	void setRow(int row) {
		this.row = row;
	}

	void setCol(int col) {
		this.col = col;
	}

	void setToken(Token token) {
		this.token = token;
	}

	int[] getAddress() {
		return new int[] {row, col};
	}

	Color getColor() {

		return token.getColor();
	}

	@Override
	public String toString() {
		return Arrays.toString(getAddress()) +"-"+ token;
	}

	@Override
	public boolean equals(Object cell) {

		if ((cell == null) || !(cell instanceof Cell)) {
			return false;
		}

		return Arrays.equals(getAddress(), ((Cell)cell).getAddress());
	}

	@Override
	public int hashCode() {

		return (31*row) + (17*col);
	}
}
