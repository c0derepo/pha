/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.SwingPropertyChangeSupport;

/**
 *
 * Model.java
 *
 * @author c0der 4c.app.testing@gmail.com
 *
 * 15 Aug 2018
 *
 */
public class Model {

	private static int rows = 30;
	private static int cols = rows;
	private static final String TOKEN = "token";
	private List<Cell> cells;
	private SwingPropertyChangeSupport pcSupport
							= new SwingPropertyChangeSupport(this);

	//a stack representing cells in the path
	private Path path;

	/**
	 * @throws IllegalArgumentException
	 * if rows != cols
	 */
	public Model(int rows, int cols) {

		this.rows = rows ; this.cols = cols;
		if(rows != cols) { throw new IllegalArgumentException("rows != cols");}
		intitialize();
	}

	public Model() {
		intitialize();
	}

	private void intitialize() {

		cells = new ArrayList<>();

		//construct all cells with  Token.EMPTY
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				cells.add (new Cell(row, col));
			}
		}
	}

	//get cell by row col. may return null
	Cell getCell(int row, int col) {

		for(Cell cell : cells) {

			if((row == cell.getRow()) && (col == cell.getCol())) {
				return cell;
			}
		}

		return null;
	}

	Token getToken(int row, int col) {

		Cell cell = getCell(row,col);
		return  cell == null ?  null : cell.getToken();
	}

	void setToken(Token token, int row, int col) {

		Cell cell = getCell(row,col);
		if(cell == null) {
			return;
		}

		Token oldValue = cell.getToken();
		cell.setToken(token);
		int index = (row * rows) + col;

		findPath(cell);
		pcSupport.fireIndexedPropertyChange(TOKEN, index, oldValue, token);


	}

	void addPropertyChangeListener(String name, PropertyChangeListener listener) {

		pcSupport.addPropertyChangeListener(name, listener);
	}

	int getRows() {
		return rows;
	}

	int getCols() {
		return cols;
	}

	String getToken() {

		return TOKEN;
	}

	//search for a path
	private void findPath(Cell cell) {

		//initialize path and checked
		path = new Path(this);
		path.findPath(cell);
	}

	List<Cell> getPath() {

		return (path == null ) ? null : path.getPath();
	}

	List<Cell> getContainedWithin() {

		return (path == null ) ? null : path.getContainedWithin();
	}
}
