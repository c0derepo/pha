/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * a stack representing cells in the path
 *
 * Path.java
 *
 * @author c0der 4c.app.testing@gmail.com
 * 15 Aug 2018
 *
 */
class Path extends Stack<Cell>{

	private Model model;

	//a path shorter than min can not surround any cell
	private static final int MIN_PATH_LEGTH = 3;

	//a collection of cells that has been tested
	private List<Cell>checked;

	//represents the cell where the search starts from
	Cell origin;
	//represents the token of the origin
	Token originToken;

	//represents the path bounds: min/max row/col in path
	private int minPathRow, maxPathRow, minPathCol, maxPathCol;

	//a collection of all cells that are bounded by the path
	//and their token is of the opposite color of the path
	private List<Cell> containedWithin;

	//a STATIC collection that holds all containedWithin cells, of
	//current and previous paths
	private static List<Cell> allContainedWithin = new ArrayList<>();

	Path(Model model){

		this.model = model;
	}

	//search for a path
	boolean findPath(Cell origin) {

		this.origin = origin;

		//represents the token of the origin
		originToken = origin.getToken();

		//initialize list of checked items
		checked = new  ArrayList<>();

		boolean found = findPath(origin.getRow(), origin.getCol());

		if(found) {

			//find bounded cells
			findContainedWithin();
			//update the collection all
			allContainedWithin.addAll(containedWithin);
			printPath();

		} else {
			System.out.println("No path found");
		}

		return found;
	}

	//recursive method to find path. a cell is represented by its row, col
	//returns true when path was found
	private boolean findPath(int row, int col) {

		Cell cell = model.getCell(row, col);

		//check if cell is null or has the same token as origin
		if((cell == null) || (cell.getToken() != originToken)) {
			return false;
		}

		//check if this cell was tested before to avoid checking again
		if(checked.contains(cell)) {
			return false;
		}

		//check if this cell was contained in previously calculated paths
		if(allContainedWithin.contains(cell)) {
			return false;
		}

		//get cells neighbors
		List<Cell> neighbors = getNeighbors(cell);

		//check if solution found. If path size > min and cell
		//neighbors contain the origin it means that path was found
		if((size() >= MIN_PATH_LEGTH) && neighbors.contains(origin)  ) {

			add(cell);
			return true;
		}

		//add cell to checked
		checked.add(cell);

		//add cell to path
		add(cell);

		//if path was not found check cell neighbors
		for(Cell neighbor : neighbors ) {

			boolean found = findPath(neighbor.getRow(), neighbor.getCol());

			if(found) {
				return true;
			}
		}

		//path not found
		pop(); //remove last element from stack
		return false;
	}

	//return a list of all neighbors of cell
	private List<Cell> getNeighbors(Cell cell) {

		int  row = cell.getRow(),  col = cell.getCol();
		List<Cell> neighbors = new ArrayList<>();

		for (int colNum = col - 1 ; colNum <= (col + 1) ; colNum ++  ) {

			for (int rowNum = row - 1 ; rowNum <= (row + 1) ; rowNum ++  ) {

				if(!((colNum == col) && (rowNum == row))) {

					Cell neighbor = model.getCell(rowNum, colNum);
					if(neighbor != null ) {

						neighbors.add(neighbor);
					}
				}
			}
		}

		return neighbors;
	}

	//use for testing
	private void printPath() {

		System.out.print("Path : " );
		for(Cell cell : this) {
			System.out.print(cell);
		}
		System.out.println("");

		List<Cell> containedCells = getContainedWithin();

		System.out.print(containedCells.size()+" cell contained : " );
		for(Cell cell : containedCells) {
			System.out.print(cell);
		}
		System.out.println("");
	}

	List<Cell> getPath() {

		return new ArrayList<>(this);
	}

	//finds all cells that are bounded by the path
	//and their token is of the opposite color of the path
	private void findContainedWithin() {

		containedWithin = new ArrayList<>();

		//find path max and min X values, max and min Y values
		minPathRow = model.getRows(); //set min to the largest possible value
		maxPathCol = model.getCols();
		maxPathRow = 0;              //set max to the largest possible value
		maxPathCol = 0;

		//find the actual min max x y values of the path
		for (Cell cell : this) {
			minPathRow = Math.min(minPathRow, cell.getRow());
			minPathCol = Math.min(minPathCol, cell.getCol());
			maxPathRow = Math.max(maxPathRow, cell.getRow());
			maxPathCol = Math.max(maxPathCol, cell.getCol());
		}

		Cell cell = get(0);//get an arbitrary cell in the path
		Token pathToken = cell.getToken(); //keep a reference to its token

		//iterate over all cells within path x, y limits
		for (int col = minPathCol; col < (maxPathCol); col++) {

			for (int row = minPathRow; row < (maxPathRow); row++) {

				Cell cCell = model.getCell(row, col);

				//check cell color
				Token token = cCell.getToken();
				if ((token == pathToken) || (token == Token.EMPTY)) {
					continue;
				}
				if (isWithinLoop(cCell)) {
					containedWithin.add(cCell);
				}
			}
		}
	}

	//returns a collection of all cells that are bounded by the path
	//and their token is of the opposite color of the path
	List<Cell> getContainedWithin() {

		return containedWithin;
	}

	//check if a cell is within path by checking if it has a
	//path-cell to its left, right, top and bottom
	private boolean isWithinLoop(Cell cell) {

		if(  isPathCellOnLeft(cell)
			 &&
			 isPathCellOnRight(cell)
			 &&
			 isPathCellOnTop(cell)
			 &&
			 isPathCellOnBottom(cell)
		  ) {
			return true;
		}

		return false;
	}

	private boolean isPathCellOnLeft(Cell cell) {

		for ( int col = minPathCol; col < cell.getCol() ; col++) {

			Cell cCell = model.getCell(cell.getRow(), col);

			if((cCell != null) && getPath().contains(cCell)){
				return true;
			}
		}

		return false;
	}

	private boolean isPathCellOnRight(Cell cell) {

		for ( int col = cell.getCol(); col <= maxPathCol ; col++) {

			Cell cCell = model.getCell(cell.getRow(), col);

			if((cCell != null) && getPath().contains(cCell)) {
				return true;
			}
		}

		return false;
	}

	private boolean isPathCellOnTop(Cell cell) {

		for ( int row = minPathRow; row < cell.getRow() ; row++) {

			Cell cCell = model.getCell(row, cell.getCol());

			if((cCell != null) && getPath().contains(cCell)) {
				return true;
			}
		}

		return false;
	}

	private boolean isPathCellOnBottom(Cell cell) {

		for ( int row = cell.getRow(); row <= maxPathRow; row++) {

			Cell cCell = model.getCell(row, cell.getCol());

			if((cCell != null) && getPath().contains(cCell)) {
				return true;
			}
		}

		return false;
	}
}