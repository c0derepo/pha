/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * TLabel.java
 *
 * @author c0der 4c.app.testing@gmail.com
 *
 * 15 Aug 2018
 *
 */
class TLabel extends JLabel{

    private Token token;
    private int iconWidth;

    TLabel(Token token, int rows) {
    	setIconWidth(rows);
        setToken(token);
    }

    void setToken(Token token) {

        this.token = token;
        setIcon(createIcon(token.getColor()));
        setPreferredSize(new Dimension(iconWidth, iconWidth));
    }

    Token getToken() {
        return token;
    }

    Color getColor() {
        return token.getColor();
    }

	private void setIconWidth(int rows) {

		if(rows <= 20) {
			iconWidth = 24;
		} else if( rows <= 40 ) {
			iconWidth = 20;
		} else if( rows <= 60 ) {
			iconWidth = 15;
		} else if( rows <= 80 ) {
			iconWidth = 12;
		} else {
			iconWidth = 8;
		}
	}

    private Icon createIcon(Color color) {

        BufferedImage img = new BufferedImage(iconWidth, iconWidth,
        									BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img.createGraphics();
        g2.setColor(color);
        g2.fillOval(1, 1, iconWidth - 2, iconWidth - 2);
        g2.dispose();

        return new ImageIcon(img);
    }
}