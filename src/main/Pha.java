/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.SwingUtilities;
/**
 *
 * Pha.java
 *
 * @author c0der 4c.app.testing@gmail.com
 * 15 Aug 2018
 *
 */
public class Pha {

	Pha(){
		new Control(new Model(), new View());
	}

	Pha(int rows, int cols){

		Model model = new Model(rows, cols);
		View view = new View();
		new Control(model, view);
	}

	public static void main(String[] args) {

		// run all on the Swing event thread
		SwingUtilities.invokeLater(() -> {
			new Pha();
		});
	}
}
