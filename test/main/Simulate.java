/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.SwingWorker;

/**
 * Simulate.java
 */
public class Simulate {

	private static final int EMPTY = 0, BLUE = 1, RED = 2;
	Token[][] tokens; Model model;

	Simulate(Token[][] tokens) {
		this.tokens = tokens;
		model = new Model(tokens.length, tokens[0].length);
		View view = new View();
		new Control(model, view);

	}

	void loadTokens() {
		new ChangeTokens(tokens, model).run();
	}

	public static void main(String[] args) {

		int[][] gameBoard = getTestData52x52();
		//or use same data as above, removing empty lines / rows
		//int[][] gameBoard = getTestData6x6();

		int size = gameBoard.length;
		Token[][] tokens  =  new Token[size][size];
		for (int i = 0; i < size; i++ ) {

			for (int j = 0; j < size; j++) {
				tokens[i][j] = Token.values()[gameBoard[i][j]];
			}
		}

		new Simulate(tokens).loadTokens();
	}

	private static int[][] getTestData52x52() {

		int size = 52;
		int[][] gameBoard = new int[size][size];

		gameBoard[47][47]= RED;
		gameBoard[46][47]= BLUE;
		gameBoard[44][48]= RED;
		gameBoard[44][49]= RED;
		gameBoard[45][47]= RED;
		gameBoard[45][48]= BLUE;
		gameBoard[45][49]= BLUE;
		gameBoard[45][50]= RED;
		gameBoard[46][50]= BLUE;
		gameBoard[46][49]= RED;
		gameBoard[46][48]= RED;
		gameBoard[47][50]= BLUE;
		gameBoard[47][48]= BLUE;
		gameBoard[47][49]= RED;
		gameBoard[48][50]= RED;
		gameBoard[48][49]= RED;
		gameBoard[48][48]= RED;
		gameBoard[49][50]= BLUE;
		gameBoard[48][51]= RED;
		gameBoard[44][50] = BLUE;

		return gameBoard;
	}

	private static int[][] getTestData6x6() {

		int size = 6;
		int[][] gameBoard = new int[size][size];

		gameBoard[3][0]= RED;
		gameBoard[2][0]= BLUE;
		gameBoard[0][1]= RED;
		gameBoard[0][2]= RED;
		gameBoard[1][0]= RED;
		gameBoard[1][1]= BLUE;
		gameBoard[1][2]= BLUE;
		gameBoard[1][3]= RED;
		gameBoard[2][3]= BLUE;
		gameBoard[2][2]= RED;
		gameBoard[2][1]= RED;
		gameBoard[3][3]= BLUE;
		gameBoard[3][1]= BLUE;
		gameBoard[3][2]= RED;
		gameBoard[4][3]= RED;
		gameBoard[4][2]= RED;
		gameBoard[4][1]= RED;
		gameBoard[5][3]= BLUE;
		gameBoard[4][4]= RED;
		gameBoard[4][3] = BLUE;

		return gameBoard;
	}
}

class ChangeTokens extends SwingWorker<Void, Void>{

	private Token[][] tokens;
	private Model model;
	private static final long wait = 1000;

	ChangeTokens(Token[][] tokens, Model model) {
		this.tokens = tokens;
		this.model = model;
	}

	/**
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {

		for (int i = 0; i < tokens.length; i++) {
			for (int j = 0; j <tokens[i].length; j++) {
				//skip emtpy
				if(tokens[i][j] == Token.EMPTY) {
					continue;
				}
				model.setToken(tokens[i][j], i, j);
				Thread.sleep(wait);
			}
		}

		return null;
	}
}
